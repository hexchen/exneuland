# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

# Configures the endpoint
config :exneuland, ExneulandWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: ExneulandWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Exneuland.PubSub,
  live_view: [signing_salt: "1Z/WAcss"],
  http: [
    dispatch: [
      {:_,
       [
         {"/room", ExneulandWeb.SocketHandler, []},
         {:_, Phoenix.Endpoint.Cowboy2Handler, {ExneulandWeb.Endpoint, []}}
       ]}
    ]
  ]

config :exneuland, Exneuland.Backend, adapter: CCCV.Exneuland.Backend

config :exneuland,
  token_secret: System.get_env("EXNEU_SECRET", "secret"),
  group_radius: 48,
  group_members: 5

config :exneuland, CCCV.Exneuland.Backend,
  hub_url: System.get_env("EXNEU_HUBURL", "https://hub-staging.cccv.de"),
  hub_token: System.get_env("EXNEU_HUBTOKEN", ""),
  event_slug: System.get_env("EXNEU_SLUG", "rc3_21"),
  user_tables: [:badges, :config],
  map_tables: [:badges]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :libcluster,
  topologies: [
    main: [
      strategy: Cluster.Strategy.Kubernetes,
      config: [
        mode: :dns,
        kubernetes_node_basename: "exneuland",
        kubernetes_selector: "app=exneuland",
        kubernetes_ip_lookup_mode: :pods,
        kubernetes_namespace: System.get_env("NAMESPACE", "staging"),
        polling_interval: 10_000
      ]
    ]
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
