import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :exneuland, ExneulandWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "lJBujDKSF+6AsbaheIby/j+0C5YEDIzMg8tNPjahyt7uaar2KUE+R5OB381EFBs0",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

config :libcluster,
  topologies: [
    main: [
      strategy: Elixir.Cluster.Strategy.LocalEpmd
    ]
  ]
