#!/usr/bin/env bash

# Extract the host where the server is running, and add the URL to the APIs
[[ $HOST =~ ^https?://[^/]+ ]] && HOST="${BASH_REMATCH[0]}/api/v4/projects/"

echo "[DEBUG] * Host: $HOST"

# Look which is the default branch
TARGET_BRANCH=`curl --silent "${HOST}${DEPLOY_PROJECT_ID}" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" | python3 -c "import sys, json; print(json.load(sys.stdin)['default_branch'])"`;

echo "[DEBUG] * target branch: $TARGET_BRANCH"

# The description of our new MR, we want to remove the branch after the MR has
# been closed
BODY="{
    \"id\": ${DEPLOY_PROJECT_ID},
    \"source_branch\": \"${SOURCE_BRANCH}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"remove_source_branch\": true,
    \"title\": \"${MR_TITLE}\",
    \"assignee_id\":\"${ASSIGNEE_USER_ID}\"
}";

echo "[DEBUG] * body: $BODY"

# Require a list of all the merge request and take a look if there is already
# one with the same source branch
LISTMR=`curl -L --silent "${HOST}${DEPLOY_PROJECT_ID}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}"`;
COUNTBRANCHES=`echo ${LISTMR} | grep -o "\"source_branch\":\"${SOURCE_BRANCH}\"" | wc -l | xargs`;

echo "[DEBUG] * MRs: $LISTMR"
echo "[DEBUG] * Count: $COUNTBRANCHES"

# No MR found, let's create a new one
if [ ${COUNTBRANCHES} -eq "0" ]; then
    curl -X POST "${HOST}${DEPLOY_PROJECT_ID}/merge_requests" \
        --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "${BODY}";

    echo "Opened a new merge request: ${MR_TITLE} and assigned to you";
    exit;
fi

echo "No new merge request opened";
