{
  description = "Exneuland: A workadventure backend";

  inputs.flake-compat = {
    url = "github:edolstra/flake-compat";
    flake = false;
  };

  outputs = { self, nixpkgs, ... }:
    let
      version = self.shortRev or (toString self.lastModifiedDate);
      overlay = final: prev: {
        exneuland = final.callPackage ({ beam, protobuf, rebar3 }:
          let
            packages = beam.packagesWith beam.interpreters.erlang;
            pname = "exneuland";
            src = self;
            mixEnv = "prod";
            mixDeps = packages.fetchMixDeps {
              pname = "mix-deps-${pname}";
              inherit src mixEnv version;
              sha256 = "sha256-edpx7KhxZK3oSA+/8T6xATwjxU2hHZW/jHDbQ30xivY=";
            };

          in packages.mixRelease {
            inherit pname version src mixEnv;
            mixFodDeps = mixDeps;

            nativeBuildInputs = [ rebar3 protobuf ];

            postBuild = ''
              mix phx.digest --no-deps-check
            '';
          }) { };
      };

      systems =
        [ "x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];
      forAllSystems = f: nixpkgs.lib.genAttrs systems (system: f system);

      # Memoize nixpkgs for different platforms for efficiency.
      nixpkgsFor = forAllSystems (system:
        import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        });

    in {
      overlay = overlay;

      packages =
        forAllSystems (system: { inherit (nixpkgsFor.${system}) exneuland; });

      defaultPackage =
        forAllSystems (system: self.packages.${system}.exneuland);
    };
}
