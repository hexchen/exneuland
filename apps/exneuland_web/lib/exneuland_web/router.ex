defmodule ExneulandWeb.Router do
  use ExneulandWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug ExneulandWeb.Plugs.API.Auth
  end

  pipeline :wa do
    plug :accepts, ["json"]
  end

  pipeline :wa_auth do
    plug ExneulandWeb.Plugs.Auth
  end

  scope "/api", ExneulandWeb.Controllers.API do
    pipe_through :api

    post "/maps/list", MapPushController, :index

    post "/users/:uuid", UserController, :update
    delete "/users/:uuid", UserController, :delete
  end

  scope "/", ExneulandWeb.Controllers do
    pipe_through :wa

    post "/register", AuthController, :register
    post "/anonymLogin", AuthController, :anonym_login
    get "/logout-callback", AuthController, :logout_callback

    get "/map", MapController, :index
  end

  scope "/", ExneulandWeb.Controllers do
    pipe_through [:wa, :wa_auth]

    get "/verify", AuthController, :verify
    get "/login-callback", AuthController, :login_callback
  end

  scope "/", ExneulandWeb.Controllers, log: false do
    get "/metrics", MetricController, :index
    get "/ready", MetricController, :ready
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: ExneulandWeb.Telemetry
    end
  end
end
