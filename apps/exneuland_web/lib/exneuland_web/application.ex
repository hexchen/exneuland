defmodule ExneulandWeb.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      ExneulandWeb.Telemetry,
      # Start the Endpoint (http/https)
      ExneulandWeb.Endpoint
      # Start a worker by calling: ExneulandWeb.Worker.start_link(arg)
      # {ExneulandWeb.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ExneulandWeb.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    ExneulandWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
