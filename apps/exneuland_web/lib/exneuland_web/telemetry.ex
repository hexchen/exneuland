defmodule ExneulandWeb.Telemetry do
  use Supervisor
  import Telemetry.Metrics

  @tables [
    Exneuland.Store.User,
    Exneuland.Store.Map,
    Exneuland.Store.Variable
  ]

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  @impl true
  def init(_arg) do
    children = [
      # Telemetry poller will execute the given period measurements
      # every 10_000ms. Learn more here: https://hexdocs.pm/telemetry_metrics
      {:telemetry_poller, measurements: periodic_measurements(), period: 10_000},
      # Add reporters as children of your supervision tree.
      # {Telemetry.Metrics.ConsoleReporter, metrics: metrics()}
      {TelemetryMetricsPrometheus.Core, metrics: metrics()}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end

  # TODO: more metrics usable from prometheus
  def metrics do
    [
      # Phoenix Metrics
      summary("phoenix.endpoint.stop.duration",
        unit: {:native, :millisecond}
      ),
      summary("phoenix.router_dispatch.stop.duration",
        tags: [:route],
        unit: {:native, :millisecond}
      ),
      distribution(
        "phoenix.endpoint.stop.duration",
        unit: {:native, :second},
        measurement: :duration,
        reporter_options: [
          buckets: [0.00001, 0.01, 0.025, 0.05, 0.1, 0.2, 0.5, 1]
        ]
      ),

      # TODO: jwt metrics

      # API Metrics
      counter("exneuland.api.maps.refreshed"),

      # Store Metrics
      last_value("exneuland.store.maps.count"),
      last_value("exneuland.store.users.count"),
      last_value("exneuland.store.variables.count"),

      # Socket Metrics
      counter("exneuland.cowboy.init.count", tags: [:room]),
      counter("exneuland.cowboy.terminate.count", tags: [:room]),

      # VM Metrics
      summary("vm.memory.total", unit: {:byte, :kilobyte}),
      summary("vm.total_run_queue_lengths.total"),
      summary("vm.total_run_queue_lengths.cpu"),
      summary("vm.total_run_queue_lengths.io"),
      last_value("vm.memory.total", unit: {:byte, :kilobyte}),
      last_value("vm.total_run_queue_lengths.total"),
      last_value("vm.total_run_queue_lengths.cpu"),
      last_value("vm.total_run_queue_lengths.io")
    ] ++ Exneuland.Backend.metrics()
  end

  defp periodic_measurements do
    [
      # A module, function and arguments to be invoked periodically.
      # This function must call :telemetry.execute/3 and a metric must be added above.
      # {ExneulandWeb, :count_users, []}
      {__MODULE__, :count_tables, []}
    ]
  end

  def count_tables do
    @tables
    |> Stream.map(fn table ->
      table.emit_count()
    end)
    |> Stream.run()
  end
end
