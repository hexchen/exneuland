defmodule ExneulandWeb.Plugs.API.Auth do
  @behaviour Plug
  import Plug.Conn
  alias Exneuland.Token

  @spec init(Plug.opts()) :: Plug.opts()
  @impl Plug
  def init(opts), do: opts

  @spec call(Plug.Conn, Plug.opts()) :: Plug.Conn
  @impl Plug
  def call(conn, _opts) do
    with headers <- conn.req_headers,
         token when is_binary(token) <- find_auth(headers),
         {:ok, claims} <- Token.verify_backend_token(token) do
      conn
      |> assign(:jwt_auth_claims, claims)
    else
      _ ->
        conn
        |> put_status(:unauthorized)
        |> Phoenix.Controller.json(%{error: "Unauthenticated"})
        |> halt()
    end
  end

  @spec find_auth([{String, String}]) :: String | nil
  defp find_auth([{"authorization", token} | _tail]), do: token
  defp find_auth([_head | tail]), do: find_auth(tail)
  defp find_auth([]), do: nil
end
