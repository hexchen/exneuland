defmodule ExneulandWeb.Plugs.Auth do
  @behaviour Plug
  import Plug.Conn
  alias Exneuland.Token

  @moduledoc """
  Authenticate the user based on the Query parameter `token`.
  Stores the user in `:user`
  """

  @spec init(Plug.opts()) :: Plug.opts()
  @impl Plug
  def init(opts), do: opts

  @spec call(Plug.Conn, Plug.opts()) :: Plug.Conn
  @impl Plug
  def call(conn, _opts) do
    with token when is_binary(token) <- Map.get(conn.params, "token"),
         {:ok, claims, user} <- Token.verify_user_token(token) do
      conn
      |> assign(:user, user)
      |> assign(:jwt_auth_claims, claims)
    else
      # {:error, e} ->
      _ ->
        conn
        |> put_status(:unauthorized)
        |> Phoenix.Controller.json(%{error: "Token Invalid"})
        |> halt()
    end
  end
end
