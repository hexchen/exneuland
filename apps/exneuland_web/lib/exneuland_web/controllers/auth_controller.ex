defmodule ExneulandWeb.Controllers.AuthController do
  use ExneulandWeb, :controller
  alias Exneuland.Store.User

  def verify(conn, %{}) do
    conn
    |> json(%{success: true})
  end

  @spec register(Plug.Conn, Map) :: Plug.Conn
  @doc """
  Register a user to the backend with the given `organizationMemberToken`
  """
  def register(conn, %{"organizationMemberToken" => token}) do
    case User.register_user(token) do
      {:ok, user, room, jwt} ->
        conn
        |> json(%{
          userUuid: user.uuid,
          name: user.nickname,
          roomUrl: room,
          textures: user.textures,
          authToken: jwt,
          mapUrlStart: "",
          email: "#{user.nickname}@users.rc3.world",
          organizationMemberToken: nil
        })
      {:error, {:status_code, 404}} ->
        conn
        |> put_status(:unauthorized)
        |> json(%{error: "Unauthorized"})

      {:error, {:status_code, _status_code}} ->
        conn
        |> put_status(:internal_server_error)
        |> json(%{error: "Internal Server error"})
    end
  end

  @spec anonym_login(Plug.Conn, Map) :: Plug.Conn
  @doc """
  Anonymously log in to the WA.

  ## TODO
  Currently disabled, so returns unauthorized
  """
  def anonym_login(conn, _params) do
    conn
    |> put_status(:unauthorized)
    |> json(%{error: "Anonymous login is disabled."})
  end

  @spec login_callback(Plug.Conn, Map) :: Plug.Conn
  @doc """
  Callback for WA to verify login
  """
  def login_callback(conn, %{"token" => token}) do
    user = conn.assigns.user

    conn
    |> json(%{
      authToken: token,
      userUuid: user.uuid,
      textures: user.textures,
      # TODO: fetch mail end from backend?
      email: "#{user.nickname}@exneuland"
    })
  end

  @spec logout_callback(Plug.Conn, Map) :: Plug.Conn
  @doc """
  Callback for WA to verify logout succeded

  ## TODO
  Currently unimplemented, just returns 200
  """
  def logout_callback(conn, _params) do
    conn
    |> send_resp(200, "")
  end
end
