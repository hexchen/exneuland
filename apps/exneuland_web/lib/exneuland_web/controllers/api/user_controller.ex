defmodule ExneulandWeb.Controllers.API.UserController do
  use ExneulandWeb, :controller
  require Logger
  alias Exneuland.Store.User
  alias Exneuland.Backend
  alias Exneuland.PubSub

  def update(conn, %{"uuid" => uuid}) do
    old_user = Exneuland.Store.User.fetch_user(uuid)

    with {:ok, user} <- Backend.parse_user(conn.body_params),
         {:ok, _} <- write_user(user, uuid) do
      if old_user == nil || (old_user.config != user.config and user.config != nil) do
        Exneuland.PubSub.send_user(uuid, {:config_update, user.config})
      end

      conn
      |> send_resp(204, "")
    else
      {:error, err} ->
        conn
        |> put_status(500)
        |> json(%{error: inspect(err)})

      _ ->
        conn
        |> put_status(500)
        |> json(%{})
    end
  end

  def delete(conn, %{"uuid" => uuid}) do
    Memento.transaction(fn ->
      Memento.Query.delete(User, uuid)
    end)
    |> case do
      :ok ->
        :ok

      v ->
        Logger.error("Failed to kick user from database: #{inspect(v)}")
    end

    PubSub.broadcast("rooms:all", {:kick, uuid})

    conn
    |> send_resp(204, "")
  end

  defp write_user(user, _uuid) do
    Memento.transaction(fn ->
      user
      |> Memento.Query.write()
    end)
  end
end
