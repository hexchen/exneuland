defmodule ExneulandWeb.Controllers.API.MapPushController do
  use ExneulandWeb, :controller

  def index(conn, _params) do
    count = Exneuland.Store.Map.parse_maps(conn.body_params)

    :telemetry.execute([:exneuland, :api, :maps, :refreshed], %{})

    conn
    |> json(%{count: count})
  end
end
