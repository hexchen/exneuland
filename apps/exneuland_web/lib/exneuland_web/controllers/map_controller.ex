defmodule ExneulandWeb.Controllers.MapController do
  use ExneulandWeb, :controller
  alias Exneuland.Store

  def index(conn, %{"playUri" => playUri}) do
    with {:ok, map} <- Store.Map.fetch_map_url(playUri) do
      {org, community, url} = map.key
      room_slug = "#{org}/#{community}/#{url}"

      response = %{
        roomSlug: room_slug,
        mapUrl: map.url,
        policy_type: 0,
        tags: [],
        textures: [],
        group: nil
      }

      conn
      |> json(response)
    else
      {:error, :not_found} ->
        conn
        |> put_status(:not_found)
        |> json(%{error: "Map not found"})

      {:error, :invalid_type} ->
        conn
        |> put_status(400)
        |> json(%{error: "Invalid map type"})

      _ ->
        conn
        |> put_status(500)
        |> json(%{
          error: "Internal error"
        })
    end
  end
end
