defmodule ExneulandWeb.Controllers.MetricController do
  use ExneulandWeb, :controller

  def index(conn, %{}) do
    conn
    |> put_resp_content_type("text/plain; version=0.0.4")
    |> put_req_header("Cache-Control", "no-cache")
    |> send_resp(
      200,
      TelemetryMetricsPrometheus.Core.scrape()
    )
  end

  def ready(conn, %{}) do
    if Exneuland.Cluster.ReadyState.is_ready?() do
      conn
      |> send_resp(200, "ready")
    else
      conn
      |> send_resp(500, "Not ready")
    end
  end
end
