defmodule ExneulandWeb.SocketHandler do
  @moduledoc """
  The functions in this module implement the :cowboy_websocket behaviour, which
  handles a single websocket connection in one process of the VM. Each of these
  processes will be registered in appropriate registries so that others can find
  it (`Exneuland.Groups`, `Exneuland.Players`, and `Exneuland.Rooms`).

  Note that most of the actual logic is done in `Exneuland.MessageHandler`; this
  module handles only the initial setup as well as message parsing and (queued)
  message sending.

  For documentation on the :cowboy_websocket behaviour, refer to
  [ninenines.eu](https://ninenines.eu/docs/en/cowboy/2.7/manual/cowboy_websocket/).
  """
  @behaviour :cowboy_websocket
  require Logger
  alias Exneuland.PubSub
  alias Exneuland.Messages
  import Exneuland.Messages, only: [send_proto: 2, queue: 2]

  defp parse_int(x) when is_bitstring(x), do: Integer.parse(x)
  defp parse_int(_x), do: :error

  @doc """
  Builds an initial state from the client's first request,
  which is then passed to `websocket_init/1`
  """
  def init(request, _state) do
    args =
      request.qs
      |> String.replace("characterLayers", "characterLayers[]")
      |> String.replace("characterLayer=", "characterLayers[]=")
      |> Plug.Conn.Query.decode()
      |> Enum.map(fn {k, v} ->
        case parse_int(v) do
          :error -> {k, v}
          {x, _} -> {k, x}
        end
      end)
      |> Map.new()

    with {:ok, claims, user} <- Exneuland.Token.verify_user_token(args["token"]),
         {:ok, map} <- Exneuland.Store.Map.fetch_map_url(args["roomId"]) do
      character_layers =
        args
        |> Map.get("characterLayers", [])

      if character_layers != user.layers do
        Exneuland.Backend.update_user(user, %{"characterLayers" => character_layers})
      end

      viewport = Messages.struct(ViewPortMessage,
        left: args["left"],
        top: args["top"],
        right: args["right"],
        bottom: args["bottom"]
      )

      position = Messages.struct(PositionMessage,
        x: args["x"],
        y: args["y"],
        direction: :DOWN,
        moving: false
      )

      char_layers =
        args["characterLayers"]
        |> Enum.map(&Messages.struct(CharacterLayerMessage, name: &1))

      state = %{
        room: args["roomId"],
        name: user.nickname,
        token: args["token"],
        character: char_layers,
        companion: Messages.struct(CompanionMessage, name: Map.get(args, "companion", "")),
        position: position,
        viewport: viewport,
        silent: false,
        uid: Enum.random(0..2_147_483_647),
        uuid: claims["userUuid"],
        queue: [],
        peers: [],
        group: nil,
        # badges: map.badges
        private: %{}
      }

      case Exneuland.Backend.init_state(state, user, map, args) do
        {:ok, state} ->
          {:cowboy_websocket, request, {state, user}}
        _ ->
          {:cowboy_websocket, request, :invalid}
      end
    else
      _ -> {:cowboy_websocket, request, :invalid}
    end
  end

  def websocket_init(:invalid) do
    reply = Messages.s2c(
      {:tokenExpiredMessage, Messages.struct(TokenExpiredMessage)}
    )

    # reply = %Messages.ServerToClientMessage{message: {:worldConnexionMessage, %Messages.WorldConnexionMessage{message: "no access"}}}
    case Messages.encode(reply) do
      {:ok, binmsg} ->
        {[{:binary, binmsg}, :close], :invalid}
      _ -> {[:close], :invalid}
    end
  end

  @doc """
  At this point, the connection was updated to be a websocket.
  Registers this process in the appropriate registries,
  broadcasts an announcement about the new client to others,
  and sends a response to this one.
  """
  def websocket_init({state, user}) do
    PubSub.subscribe(state.room)
    PubSub.subscribe("user:#{state.uid}")
    PubSub.subscribe("user:#{state.uuid}")
    PubSub.subscribe("rooms:all")

    :telemetry.execute([:exneuland, :cowboy, :init], %{}, %{room: state.room})

    Process.flag(:trap_exit, true)

    user_settings = CCCV.Exneuland.User.UserConfig.build_user_settings_wa(state.name, user.config)

    reply =
      Messages.struct(RoomJoinedMessage, currentUserId: state.uid, userSettings: user_settings)
      |> (fn m -> Messages.s2c({:roomJoinedMessage, m}) end).()

    PubSub.broadcast(
      state.room,
      {:hello, Messages.user_joined_from_state(state)}
    )

    send_proto(reply, state)
  end

  @doc """
  The connection to the client was closed; broadcast a leave message to others.
  """
  def terminate(_reason, _preq, %{uid: uid, room: room}) do
    :telemetry.execute([:exneuland, :cowboy, :terminate], %{}, %{room: room})

    leave = {:userLeftMessage, Messages.struct(UserLeftMessage, userId: uid)}
    PubSub.broadcast(room, leave)
    Logger.info("Sending terminate message: #{uid}")
    :ok
  end

  def terminate(_reason, _preq, _state), do: :invalid

  @doc """
  Incoming binary messages are decoded and handled via
  `Exneuland.MessageHandler.handle/2`; messages that can't be decoded via
  protobuf or are not binary are logged and otherwise ignored.
  """
  def websocket_handle({:binary, message}, state) do
    case Messages.decode_c2s(message) do
      {:ok, msg} ->
        Exneuland.Backend.handle_message(msg.message, state)
      err ->
        Logger.error("couldn't decode incoming msg: #{inspect(err)}")
        {:ok, state}
    end
  end

  def websocket_handle({:ping, _}, state), do: {:ok, state}

  def websocket_handle(msg, state) do
    Logger.warn("unexpected message! #{inspect(msg)}")
    {:ok, state}
  end

  @doc """
  called for messages sent from other processes in the VM.

  Has two modes: if `msg` is a tuple `{mode, msg}` where `mode` is one of
  `:send` `:proto` `:queue` or `flush`, the message will be forwarded via
  websocket directly to the client (using a queue controlled by `mode`).any()

  Otherwise it is passed to `Exneuland.InfoHandler.handle/2`, which translates
  the higher-level messages sent by other processes into a lower-level protobuf
  message, and then send that.
  """
  def websocket_info({:send, msg}, state), do: {:reply, msg, state}

  def websocket_info({:proto, msg}, state), do: send_proto(msg, state)

  def websocket_info({:queue, msg}, state), do: queue(msg, state)

  def websocket_info(:flush, state), do: flush(state)

  def websocket_info({:backend, msg}, state) do
    Exneuland.Backend.handle_backend(msg, state)
  end

  def websocket_info(msg, state) do
    # IO.inspect(msg)
    Exneuland.InfoHandler.handle(msg, state)
  end


  # sends all messages that are in the queue to the client
  defp flush(state) do
    batch_payload =
      state.queue
      #|> Enum.map(&%Messages.SubMessage{message: &1})
      |> Enum.map(&Messages.struct(SubMessage, message: &1))
      |> Enum.reverse()

    batch_msg = Messages.struct(BatchMessage, payload: batch_payload)
    msg = Messages.s2c({:batchMessage, batch_msg})
      #%Messages.ServerToClientMessage{message: {:batchMessage, batch_msg}}
    websocket_info({:proto, msg}, %{state | queue: []})
  end

end
