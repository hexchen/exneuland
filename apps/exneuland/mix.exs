defmodule Exneuland.MixProject do
  use Mix.Project

  def project do
    [
      app: :exneuland,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.12",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      releases: [
        exneuland: [
          config_providers: [{Exneuland.Config.ReleaseRuntimeProvider, []}]
        ]
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Exneuland.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix_pubsub, "~> 2.0"},
      {:jason, "~> 1.2"},
      {:uuid, "~> 1.1"},
      {:joken, "~> 2.0"},
      {:memento, "~> 0.3.2"},
      {:sentry, "~> 8.0"},
{:hackney, "~> 1.8"},
      {:libcluster, "~> 3.3"},
      {:protox, "~> 1.4"},
	  {:telemetry_metrics, "~> 0.6"},
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get"],
      sentry_recompile: ["compile", "deps.compile sentry --force"]
    ]
  end
end
