defmodule Exneuland.BackendSupervisor do
  use Supervisor

  def start_link(opts) do
    opts =
      opts
      |> Keyword.put_new(:name, __MODULE__)

    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  @impl Supervisor
  def init(:ok) do
    children = Exneuland.Backend.supervisor_children()

    Supervisor.init(children, strategy: :one_for_one)
  end
end
