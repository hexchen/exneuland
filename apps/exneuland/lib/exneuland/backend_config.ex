defmodule Exneuland.BackendConfig do
  @backend_adapter Application.compile_env(:exneuland, Exneuland.Backend)[:adapter]

  def get_env() do
    Application.get_env(:exneuland, @backend_adapter, [])
  end

  def fetch_env(key, default \\ nil) do
    Keyword.get(get_env(), key, default)
  end

  def fetch_env!(key) do
    fetch_env(key)
    |> case do
      nil ->
        raise(
          ArgumentError,
          "could not fetch backend config #{inspect(key)} for backend #{inspect(@backend_adapter)} because configuration at #{inspect(key)} was not set"
        )

      v ->
        v
    end
  end

  def user_tables() do
    Keyword.get(get_env(), :user_tables, [])
  end

  def map_tables() do
    Keyword.get(get_env(), :map_tables, [])
  end
end
