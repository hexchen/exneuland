defmodule Exneuland.Messages do
  import Kernel, except: [struct: 2, struct: 1]
  require Logger

  @callback wrap_batch_s2c(term) :: term
  def wrap_batch_s2c(msg),
    do: get_module().wrap_batch_s2c(msg)

  @callback user_joined_from_state(term) :: term
  def user_joined_from_state(state),
    do: get_module().user_joined_from_state(state)

  @spec get_module() :: Atom
  @doc """
  Get the currently configured Messages module from the application
  """
  def get_module(),
      do: Exneuland.BackendConfig.fetch_env(:messages, Exneuland.VanillaMessages)

  @spec distance(number, number) :: number
  @doc """
  Calculate the distance between `a` and `b`.
  """
  def distance(a, b) do
    import :math
    round(sqrt(pow(b.x - a.x, 2) + pow(b.y - a.y, 2)))
  end

  @spec middle([term]) :: {number, number}
  @doc """
  Calculate the middle of positions
  """
  def middle(positions) do
    {xsum, ysum} = List.foldl(positions, {0, 0}, fn pos, {x, y} -> {x + pos.x, y + pos.y} end)
    l = length(positions)
    {round(xsum / l), round(ysum / l)}
  end

  @spec struct(Atom, Keyword) :: term
  def struct(name, fields \\ []) do
    name = Module.concat(get_module(), name)
    Kernel.struct(name, fields)
  end

  @doc false
  def s2c(msg) do
    struct(ServerToClientMessage, message: msg)
  end

  @doc """
  Encodes a protobuf message and sends it over the websocket.
  This returns a socket handler return value. So return the return value to cowboy.
  """
  def send_proto(msg, state) do
    with {:ok, binmsg} <- encode(msg) do
      {:reply, {:binary, binmsg}, state}
    else
      err ->
        Logger.error("couldn't encode outgoing msg: #{inspect(err)}, #{inspect(msg)}")
        {:ok, state}
    end
  end

  @doc """
  adds either a single message or a list of messages to the message queue
  attribute of the `state` tuple (used to send messages to the client in bulk).
  """
  def queue(msgs, state) when is_list(msgs) do
    if state.queue == [] do
      Process.send_after(self(), :flush, 200)
    end

    {:ok, %{state | queue: msgs ++ state.queue}}
  end

  def queue(msg, state) do
    queue([msg], state)
  end

  def decode_c2s(msg) do
    c2sm = Module.concat(get_module(), ClientToServerMessage)
    c2sm.decode(msg)
  end

  defdelegate encode(msg), to: Protox

  # --- using macro and helpers ---
  defmacro __using__(opts) do
    files = Keyword.fetch!(opts, :files)
    namespace = Keyword.fetch!(opts, :namespace)
    quote do
      @behaviour unquote(__MODULE__)
      use Protox, files: unquote(files_array(files)), namespace: unquote(namespace)

      def foo(x), do: x
    end
  end

  defp files_array(file) when is_binary(file) do
    [file]
  end
  defp files_array(file) when is_list(file) do
    file
  end

end
