defmodule Exneuland.InfoHandler do
  require Logger
  alias Exneuland.Messages
  alias Exneuland.PubSub

  @moduledoc """
    Handles "info" messages for the SocketHandler, i.e. messages that are not
    websocket messages sent from the client, but messages sent within the VM
    from another process.
  """

  @doc """
  translates a higher-level message into the corresponding protobuf message,
  using information from `state` if needed.
  """
  # new client joined; leaves `state` unchanged
  def handle({:hello, msg}, state) do
    reply = {:userJoinedMessage, Exneuland.Messages.user_joined_from_state(state)}

    PubSub.broadcast(state.room, reply)

    Messages.queue({:userJoinedMessage, msg}, state)
  end

  # some other client moved; if it is close enough, start a group
  # with it; leaves the state unchanged.
  def handle({:userMovedMessage, msg} = cmsg, state) do
    dist = Messages.distance(msg.position, state.position)
    moving = msg.position.moving or state.position.moving

    if dist <= Exneuland.Group.group_radius() and !state.silent and !moving and state.group == nil do
      reply = {:initGroup, {state.uid, state.name, state.position}}
      Logger.debug("Found other user in reach for group")
      PubSub.send_user(msg.userId, reply)
    else
      nil
    end

    Messages.queue(cmsg, state)
  end

  # some other client wants to talk to ours; register a group with a random ID,
  # and add it to our state.
  def handle({:initGroup, {peer_uid, _, _} = peer}, state) when is_number(peer_uid) do
    if state.group == nil and !state.silent do
      gid = Enum.random(0..2_147_483_647)
      Exneuland.Group.start(state.room, gid, [{state.uid, state.name, state.position}, peer])
      # PubSub.subscribe("group:#{gid}")
      {:ok, %{state | group: gid}}
    else
      {:ok, state}
    end
  end

  # a group noticed us and would like us to join it.
  def handle({:joinGroup, gid}, state) do
    if state.group == nil and !state.silent and !state.position.moving do
      PubSub.send_group(gid, {:join, {state.uid, state.name, state.position}})

      {:ok, %{state | group: gid}}
    else
      {:ok, state}
    end
  end

  # webRTC connection started with another client; add it to our list of peers
  def handle({:webRtcStartMessage, msg} = cmsg, state) do
    Messages.send_proto(
      Messages.s2c(cmsg),
      %{state | peers: [msg.userId | state.peers]}
    )
  end

  # webRTC connection with another client closed; remove it from our list of peers
  def handle({:webRtcDisconnectMessage, msg} = cmsg, state) do
    group =
      case length(Enum.uniq(state.peers)) do
        1 -> nil
        _ -> state.group
      end

    Logger.info("handle webrtcDisconnectMessage: msg = #{inspect(cmsg)}")

    Messages.send_proto(
      Messages.s2c(cmsg),
      %{state | peers: Enum.reject(state.peers, &(&1 == msg.userId)), group: group}
    )
  end

  def handle({:config_update, config}, state) do
    config = CCCV.Exneuland.User.UserConfig.build_user_settings_wa(state.name, config)

    msg = Messages.s2c({:userSettings, config})

    Messages.send_proto(msg, state)
  end

  def handle({:kick, uuid}, state) do
    if state.uuid == uuid do
      # {[:close], :invalid}
      {:stop, state}
    else
      {:ok, state}
    end
  end

  def handle({:terminate_node}, state) do
    {:stop, state}
  end

  # queue things that look like they ought to be queued and somehow didn't end
  # up in Exneuland.SocketHandler.websocket_info (?)
  #
  # TODO: this explanation is almost certainly wrong, since websocket_info would
  # strip the queue-mode atom before invoking Sockethandler.queue
  def handle({t, msg}, state) when is_atom(t) do
    Messages.queue({t, msg}, state)
  end

  # log unknown messages.
  def handle(msg, state) do
    Logger.info("unknown info: #{inspect(msg)}")
    {:ok, state}
  end
end
