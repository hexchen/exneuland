defmodule Exneuland.MessageHandler do
  require Logger
  alias Exneuland.Messages
  alias Exneuland.Store
  alias Exneuland.PubSub

  @moduledoc """
  High-level message handler; this module contains the logic to deal with
  messages from clients; parsing, sending, etc. are abstracted away by
  `Exneuland.SocketHandler`.
  """

  @doc """
  handles a decoded websocket message from a client, returning an updated
  version of `state`; see comments for how different message types are handled.

  Will log the message in case we don't yet know it / haven't implemented its
  type yet.
  """
  # the player switched silent mode on or off
  def handle({:silentMessage, message}, state) do
    if state.group && message.silent do
      PubSub.send_group(state.group, {:user_silent, state.uuid})
    end

    {:ok, %{state | silent: message.silent}}
  end

  # the user moved; send new position to others
  def handle({:userMovesMessage, msg}, state) do
    update = {
      :userMovedMessage,
      Messages.struct(UserMovedMessage, userId: state.uid, position: msg.position)
    }

    PubSub.broadcast(state.room, update)

    {:ok, %{state | position: msg.position, viewport: msg.viewport}}
  end

  # this user's viewport changed (might be useful for e.g. filtering updates
  # from clients outside that viewport, though this is not currently done)
  def handle({:viewportMessage, msg}, state) do
    {:ok, %{state | viewport: msg}}
  end

  # relay ICE messages between clients (clients use these to negotiate
  # peer-to-peer webRTC connections for audio/video chats)
  def handle({:webRtcSignalToServerMessage, msg}, state),
    do: webrtc(:webRtcSignalToClientMessage, msg, state)

  def handle({:webRtcScreenSharingSignalToServerMessage, msg}, state),
    do: webrtc(:webRtcScreenSharingSignalToClientMessage, msg, state)

  def handle({:emotePromptMessage, msg}, state) do
    PubSub.broadcast(
      state.room,
      {
        :emoteEventMessage,
        Messages.struct(EmoteEventMessage, actorUserId: state.uid, emote: msg.emote)
      }
    )

    {:ok, state}
  end

  def handle({:reportPlayerMessage, msg}, state) do
    key =
      state.room
      |> Store.Map.parse_map_url()
      |> case do
        {:ok, map} -> map
        {:ok, map, _fragment} -> map
        _ -> nil
      end

    {org, community, url} = key

    report = %Exneuland.Backend.Report.User{
      reportedUserUuid: msg.reportedUserUuid,
      reporterUserUuid: state.uuid,
      comment: msg.reportComment,
      orgSlug: org,
      worldSlug: community,
      roomSlug: url
    }

    Exneuland.Backend.report_user(report)

    {:ok, state}
  end

  def handle({:reportMapMessage, msg}, state) do
    key =
      state.room
      |> Store.Map.parse_map_url()
      |> case do
        {:ok, map} -> map
        {:ok, map, _fragment} -> map
        _ -> nil
      end

    {org, community, url} = key

    report = %Exneuland.Backend.Report.Map{
      reporterUserUuid: state.uuid,
      comment: msg.reportComment,
      mapUrl: state.room,
      position: %{
        x: state.position.x,
        y: state.position.y
      },
      orgSlug: org,
      worldSlug: community,
      roomSlug: url
    }

    Exneuland.Backend.report_map(report)

    {:ok, state}
  end

  def handle({:variableMessage, msg} = variable, state) do
    Store.Variable.update(state.room, msg.name, msg.value)

    PubSub.broadcast(state.room, variable)

    Messages.queue(variable, state)
  end

  def handle({:userSettings, msg}, state) do
    settings = CCCV.Exneuland.User.UserConfig.build_user_settings_hub(msg)

    # Cache updated setting
    with user when user != nil <- Exneuland.Store.User.fetch_user(state.uuid) do
      user =
        user
        |> Map.put(:config, settings)

      Memento.transaction(fn ->
        Memento.Query.write(user)
      end)
    end

    update = %{
      "wa_userconfig" => settings
    }

    Exneuland.Backend.update_user(state.uuid, update)

    {:ok, state}
  end

  # apparently nothing happened?
  def handle(nil, state), do: {:ok, state}

  # a fallthrough for all other kinds of messages (which we haven't implemented
  # yet / might not need at all); just logs the message.
  def handle(message, state) do
    Logger.warn("Unhandled message: #{inspect(message)}")
    {:ok, state}
  end

  defp webrtc(type, msg, state) do
    if msg.receiverId in state.peers do
      inner = Messages.struct(WebRtcSignalToClientMessage,
        userId: state.uid,
        signal: msg.signal
      )

      reply = {:proto, Messages.s2c({type, inner})}

      PubSub.send_user(msg.receiverId, reply)
    else
      Logger.warn("Stray WebRTC Signal from #{state.name}@#{state.uid}")
    end

    {:ok, state}
  end
end
