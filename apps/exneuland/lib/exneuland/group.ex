defmodule Exneuland.Group do
  use GenServer, restart: :transient
  require Logger
  alias Exneuland.Messages
  alias Exneuland.PubSub

  @type room :: Map
  @type participant :: {number, String, map}

  @moduledoc """
    Manager for a single WA group. I.e. the audio/video chat bubbles.
  """

  @spec group_radius() :: number
  @doc """
  The radius in which distant a user can join the group.
  """
  def group_radius do
    Application.fetch_env!(:exneuland, :group_radius)
  end

  @spec group_members() :: number
  @doc """
  The max amount of users a Group can hold at any given time
  """
  def group_members() do
    Application.fetch_env!(:exneuland, :group_members)
  end

  @spec start(room, number, [participant]) :: DynamicSupervisor.on_start_child()
  @doc """
  Start a Group process under the group supervisor
  """
  def start(room, gid, participants) do
    DynamicSupervisor.start_child(
      Exneuland.GroupSupervisor,
      {__MODULE__, {room, gid, participants}}
    )
  end

  @spec request_join(number, number, term) :: :ok | {:error, term}
  @doc """
  Send a join Request to the uid to join the group
  """
  def request_join(gid, uid, _room) do
    PubSub.send_user(uid, {:joinGroup, gid})
  end

  @spec start_link({room, number, [participant]}) :: GenServer.on_start()
  @doc false
  def start_link(state) do
    GenServer.start_link(__MODULE__, state)
  end

  @impl GenServer
  def init({room, gid, participants}) do
    Logger.debug("starting room #{gid} with: #{inspect(participants)}")
    [{uid_a, name_a, pos_a}, {uid_b, _name_b, _pos_b}] = participants

    state = %{
      gid: gid,
      room: room,
      center: %{x: 0, y: 0},
      # Map.new([{uA, {nA, pA}}])
      participants: %{uid_a => {name_a, pos_a}}
    }

    # Registry.register(Exneuland.Group, {room, gid}, {})
    # Registry.register(Exneuland.Rooms, room, {})
    PubSub.subscribe("group:#{gid}")
    PubSub.subscribe(room)

    Process.flag(:trap_exit, true)

    request_join(gid, uid_b, room)
    # Recalculate memberships after a second, in case init didn't work
    Process.send_after(self(), :calc, 1000)

    {:ok, state}
  end

  @impl GenServer
  def handle_cast(:terminate, state) do
    {:stop, {:shutdown, :empty}, state}
  end

  @impl GenServer
  def handle_info({:join, {uid, name, position}}, state) do
    for {peer, {_pname, _ppos}} <- state.participants do
      connect(peer, uid, state.room)
      connect(uid, peer, state.room)
    end

    calc(%{state | participants: Map.put(state.participants, uid, {name, position})})
  end

  @impl GenServer
  def handle_info({:user_silent, user}, state) do
    leave(user, state)
  end

  @impl GenServer
  # Some user moved. Remove it from our list of participants if it was one before
  # and walked too far away; ask it to join this group if it wasn't a participant
  # before and now got close enough and thou group still has capacity to take this
  # user.
  def handle_info({:userMovedMessage, msg}, state) do
    dist = Messages.distance(msg.position, state.center)

    if msg.userId in Map.keys(state.participants) do
      if dist > group_radius() do
        leave(msg.userId, state)
      else
        newpart =
          Map.update!(state.participants, msg.userId, fn {name, _pos} -> {name, msg.position} end)

        calc(%{state | participants: newpart})
      end
    else
      if dist <= group_radius() and length(Map.keys(state.participants)) < group_members() do
        request_join(state.gid, msg.userId, state.room)
      end

      {:noreply, state}
    end
  end

  @impl GenServer
  # TODO: don't understand this one; it doesn't seem like anyone sends :hello to
  # a group, ever?
  def handle_info({:hello, msg}, state) do
    pos = Messages.struct(PointMessage, x: state.center.x, y: state.center.y)
    reply = {
      :groupUpdateMessage,
      Messages.struct(GroupUpdateMessage, groupId: state.git, position: pos, groupSize: 60)
    }

    PubSub.send_user(msg.userId, reply)
    {:noreply, state}
  end

  @impl GenServer
  # one of our participants left
  def handle_info({:userLeftMessage, msg}, state), do: leave(msg.userId, state)

  @impl GenServer
  # recalculate the group's circle (sent by init/3 with a delay to make sure
  # things work as expected)
  def handle_info(:calc, state), do: calc(state)

  @impl GenServer
  # ignore other message
  def handle_info(_, state), do: {:noreply, state}

  # recalculates the centre of this group (e.g. because someone moved)
  defp calc(state) do
    if length(Map.keys(state.participants)) > 1 do
      newcenter = recenter(state)
      {:noreply, %{state | center: newcenter}}
    else
      {:stop, {:shutdown, :empty}, state}
    end
  end

  # remove a participant from our state
  defp leave(uid, state) do
    newpart = Map.delete(state.participants, uid)

    for {peer, _data} <- newpart do
      disconnect(peer, uid, state.room)
      disconnect(uid, peer, state.room)
    end

    calc(%{state | participants: newpart})
  end

  @impl GenServer
  # stop this group's process
  def terminate(_reason, state) do
    # TODO: send everyone a disconnect?
    msg = {:groupDeleteMessage, Messages.struct(GroupDeleteMessage, groupId: state.gid)}
    PubSub.broadcast(state.room, msg)
  end

  defp connect(target, source, _room) do
    # PubSub.send_user(room, )
    PubSub.send_user(
      source,
      {:webRtcStartMessage,
      Messages.struct(WebRtcStartMessage, userId: target, initiator: target > source)}
    )
  end

  defp disconnect(p1, p2, _room) when is_number(p1) and is_number(p2) do
    Logger.debug("send disconnect from room server: p1: #{inspect(p1)}")

    PubSub.send_user(
      p2,
      {:webRtcDisconnectMessage, Messages.struct(WebRtcDisconnectMessage, userId: p1)}
    )
  end

  defp disconnect(p1, _p2, _room) do
    Logger.info("Group: Got disconnect request for non id: #{inspect(p1)}")
  end

  defp recenter(state) do
    {x, y} = Messages.middle(Enum.map(state.participants, fn {_uid, {_name, pos}} -> pos end))

    pos = Messages.struct(PointMessage, x: x, y: y)
    PubSub.broadcast(
      state.room,
      {
        :groupUpdateMessage,
        Messages.struct(GroupUpdateMessage, groupId: state.gid, position: pos, groupSize: Enum.count(state.participants))
      }
    )

    %{x: x, y: y}
  end
end
