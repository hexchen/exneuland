defmodule Exneuland.Backend do
  import Exneuland.BackendGen
  @type user() :: Exneuland.Store.User

  @type user_report() :: Exneuland.Backend.Report.User
  @type map_report() :: Exneuland.Backend.Report.Map

  @backend_adapter Application.compile_env(:exneuland, Exneuland.Backend)[:adapter]

  @doc """
  Fetch User from backend identified by uuid
  """
  @callback fetch_user_uuid(String) :: {:ok, user} | {:error, term}
  defdelegate fetch_user_uuid(uuid), to: @backend_adapter

  @doc """
  Register user with a register token.
  Returns the user (not yet added to internal cache) and a start room url
  """
  @callback register_user(String) :: {:ok, user(), String} | {:error, term}
  defdelegate register_user(token), to: @backend_adapter

  @doc """
  Refresh maps cache

  Use `Exneuland.Store.Map.parse_maps` To add map json list to the cache
  """
  @callback refresh_maps() :: {:ok, number} | {:error, term}
  optional_def_cb :refresh_maps

  @doc """
  Parse extra value from map file with a preparse map struct
  """
  @callback parse_map_file(Map, Exneuland.Store.Map) :: Exneuland.Store.Map
  optional_def_cb :parse_map_file, [map_json, foo]

  # Parser user for api post
  @callback parse_user(Map) :: {:ok, user} | {:error, term}
  defdelegate parse_user(json), to: @backend_adapter

  @callback update_user(user, Map) :: :ok | {:error, term}
  optional_def_cb :update_user, [user, map]

  @doc """
  Report a user
  """
  @callback report_user(user_report) :: :ok | {:error, term}
  defdelegate report_user(report), to: @backend_adapter

  @doc """
  Report a map
  """
  @callback report_map(map_report) :: :ok | {:error, term}
  defdelegate report_map(report), to: @backend_adapter

  @doc """
  Metrics to export to prometheus
  """
  @callback metrics() :: [term]
  optional_def_cb :metrics, [], []

  @doc """
  Handle message from WA frontend
  """
  @callback handle_message({atom, term}, Map) :: {:ok, Map} | {:error, term}
  defdelegate handle_message(msg, state), to: @backend_adapter

  @doc """
  Handle Messages send to backend
  """
  @callback handle_backend({atom, term}, Map) :: {:ok, Map} | {:error, term}
  defdelegate handle_backend(msg, state), to: @backend_adapter

  @doc """
  Return list of childrens to start with the Application
  """
  @deprecated "Create an indempendet supervisortree instead"
  @callback supervisor_children() :: [term]
  optional_def_cb :supervisor_children, [], []

  @doc """
  Initialize Private state of socket
  """
  @callback init_state(Map, Exneuland.Store.User, Exneuland.Store.Map, Map) ::
              {:ok, Map} | {:error, term}

  @doc """
  Gat the vcard url for a user identified by UUID
  """
  @callback vcard_url(String) :: {:ok, String} | {:error, term}
  optional_def_cb :vcard_url, uuid

  @optional_callbacks parse_map_file: 2,
                      update_user: 2,
                      handle_message: 2,
                      handle_backend: 2,
                      supervisor_children: 0,
                      init_state: 4,
                      vcard_url: 1,
                      refresh_maps: 0,
                      metrics: 0

  defmacro __using__(_opts) do
    quote do
      @behaviour unquote(__MODULE__)
      alias unquote(Exneuland.BackendConfig)

      @before_compile unquote(__MODULE__)
      @compile {:inline, handle_message: 2}
    end
  end

  defmacro __before_compile__(_env) do
    quote do
      @impl unquote(__MODULE__)
      def handle_message(msg, state) do
        Exneuland.MessageHandler.handle(msg, state)
      end

      @impl unquote(__MODULE__)
      def handle_backend(_, state) do
        {:ok, state}
      end

      @impl unquote(__MODULE__)
      def vcard_url(_), do: {:error, :not_supported}
    end
  end
end
