defmodule Exneuland.Store.MapUpdater do
  require Logger
  use GenServer

  # Force update maps after one hour of silence
  @refresh_timer 60 * 60 * 1_000

  def start_link(_ \\ []) do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def got_update() do
    (Node.list() ++ [Node.self()])
    |> Stream.map(fn node ->
      GenServer.cast({__MODULE__, node}, {:updated})
    end)
    |> Stream.run()
  end

  # TODO: only start if supported by the backend?
  @impl GenServer
  def init(state) do
    random_offset = Enum.random(1..(120 * 1_000))
    ref = Process.send_after(self(), {:update}, 120_000 + random_offset)

    state
    |> Map.put(:tref, ref)

    {:ok, state}
  end

  @impl GenServer
  def handle_cast({:updated}, state) do
    state = reset_timer(state)
    {:noreply, state}
  end

  @impl GenServer
  def handle_info({:update}, state) do
    got_update()
    Exneuland.Backend.refresh_maps()
    state = reset_timer(state)
    {:noreply, state}
  end

  @impl GenServer
  # Ignore cancel timer infos
  def handle_info({:cancel_timer, ref, _t}, state) when is_reference(ref) do
    {:noreply, state}
  end

  defp reset_timer(state) do
    :telemetry.execute([:exneuland, :api, :maps], %{last_update: current_timestamp()})

    with ref when is_reference(ref) <- Map.get(state, :tref) do
      Process.cancel_timer(ref, async: true)
    end

    random_offset = Enum.random(1..(600 * 1_000))
    ref = Process.send_after(self(), {:update}, @refresh_timer + random_offset)

    state
    |> Map.put(:tref, ref)
  end

  defp current_timestamp() do
    {mega_sec, secs, _} = :os.timestamp()
    mega_sec * 1_000_000 + secs
  end
end
