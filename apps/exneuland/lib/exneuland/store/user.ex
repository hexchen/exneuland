defmodule Exneuland.Store.User do
  # TODO: Jason Derive?
  use Memento.Table,
    attributes:
      [:uuid, :nickname, :color, :messages, :textures, :layers, :active, :tags] ++
        Exneuland.BackendConfig.user_tables()

  @type t() :: %__MODULE__{
          uuid: String,
          nickname: String,
          color: String,
          messages: [String],
          textures: [term],
          layers: [term],
          active: bool
        }

  @spec fetch_user(String) :: t | nil
  def fetch_user(uuid) do
    Memento.transaction!(fn -> Memento.Query.read(__MODULE__, uuid) end)
  end

  @spec fetch_or_invalidate(String) :: t | nil
  @doc """
  Fetch user and cache a miss if the user does not exits.
  Queries the Backend if not cached allready
  """
  def fetch_or_invalidate(uuid) do
    fetch_user(uuid)
    |> case do
      nil -> fetch_from_backend_caching(uuid)
      user -> user
    end
  end

  @spec fetch_from_backend_caching(String) :: t | nil
  def fetch_from_backend_caching(uuid) do
    :telemetry.execute([:exneuland, :user, :fetch_backend], %{})

    user =
      case Exneuland.Backend.fetch_user_uuid(uuid) do
        {:ok, user} ->
          user
        _error ->
          %Exneuland.Store.User{active: false, uuid: uuid}
      end

    Memento.transaction(fn -> Memento.Query.write(user) end)
    user
  end

  def register_user(token) do
    with {:ok, user, start_room} <- Exneuland.Backend.register_user(token),
         {:ok, jwt, _claims} <- Exneuland.Token.generate_user_token(user) do
      Memento.transaction(fn -> Memento.Query.write(user) end)

      :telemetry.execute([:exneuland, :user, :register], %{})
      {:ok, user, start_room, jwt}
    else
      err -> err
    end
  end

  @spec count_users() :: number
  def count_users() do
    Memento.transaction(fn -> Memento.Query.all(__MODULE__) |> Enum.count() end)
    |> case do
      {:ok, count} -> count
      _ -> 0
    end
  end

  @spec emit_count() :: number
  @doc """
  Emmit the current count of maps to telemetry
  """
  def emit_count() do
    count = count_users()
    :telemetry.execute([:exneuland, :store, :users], %{count: count})
    count
  end
end
