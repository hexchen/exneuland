defmodule Exneuland.Store.Map do
  require Logger

  use Memento.Table,
    attributes: [:key, :url] ++ Exneuland.BackendConfig.map_tables()

  @type key() :: {String, String, String}

  @type t() :: %__MODULE__{
          key: key,
          url: String
        }

  @spec fetch_map(String, String, String) :: %__MODULE__{} | nil
  def fetch_map(slug, community, file)
      when is_binary(slug) and is_binary(community) and is_binary(file) do
    fetch_map({slug, community, file})
  end

  @spec fetch_map(key) :: %__MODULE__{} | nil
  def fetch_map({slug, community, file} = key)
      when is_binary(slug) and is_binary(community) and is_binary(file) do
    Memento.transaction!(fn -> Memento.Query.read(__MODULE__, key) end)
  end

  @spec fetch_map(String, String, String, String) :: %__MODULE__{} | nil
  def fetch_map(slug, community, file, fragment)
      when is_binary(slug) and is_binary(community) and is_binary(file) and is_binary(fragment) do
    fetch_map({slug, community, file}, fragment)
  end

  @spec fetch_map(key, String) :: %__MODULE__{} | nil
  def fetch_map({slug, community, file} = key, fragment)
      when is_binary(slug) and is_binary(community) and is_binary(file) and is_binary(fragment) do
    case fetch_map(key) do
      nil -> nil
      map when map != nil ->
        {_, map} =
          map
          |> Map.get_and_update(:url, fn url -> {url, url <> "#" <> fragment} end)

        map
    end
  end

  @spec parse_map_url(String) :: {:ok, key} | {:ok, key, String} | {:error, atom}
  def parse_map_url(url) when is_binary(url) do
    case Regex.run(~r|https?://[a-zA-Z0-9\.\-]+(?:\:\d{1,5})?/(.)/(.+)|, url) do
      [^url, type, rem] ->

        parse_map_url(type, rem)
      _ ->
        {:error, :invalid_url}
    end
  end

  @spec parse_map_url(String, String) :: {:ok, key} | {:ok, key, String} | {:error, atom}
  def parse_map_url("@", url) when is_binary(url) do
    with [org, community, map] <- String.split(url, "/", parts: 3, trim: true),
         [map | fragment] <- String.split(map, "#", parts: 2) do
      fragment
      |> case do
        [] ->
          {:ok, {org, community, map}}

        [fragment] ->
          {:ok, {org, community, map}, fragment}
      end
    else
      _ ->
        {:error, :invalid_url}
    end
  end

  @spec parse_map_url(String, String) :: {:ok, key} | {:ok, key, String} | {:error, atom}
  def parse_map_url(_type, _url) do
    {:error, :invalid_type}
  end

  @spec fetch_map_url(String) :: {:ok, t} | {:error, :atom}
  def fetch_map_url(url) do
    url
    |> parse_map_url
    |> case do
      {:ok, key, fragment} ->
        {:ok, fetch_map(key, fragment)}

      {:ok, key} ->
        {:ok, fetch_map(key)}

      v ->
        v
    end
    |> case do
      {:ok, nil} -> {:error, :not_found}
      v -> v
    end
  end

  @spec count_maps() :: number
  def count_maps() do
    Memento.transaction(fn -> Memento.Query.all(__MODULE__) |> Enum.count() end)
    |> case do
      {:ok, count} -> count
      _ -> 0
    end
  end

  @spec emit_count() :: number
  @doc """
  Emmit the current count of maps to telemetry
  """
  def emit_count() do
    count = count_maps()
    :telemetry.execute([:exneuland, :store, :maps], %{count: count})
    count
  end

  # TODO: spawn new task for this?
  @spec parse_maps(Map) :: number
  def parse_maps(maps) do
    :telemetry.span(
      [:exneuland, :store, :maps, :parse],
      %{},
      fn ->
        parse_maps_span(maps)
        {emit_count(), %{}}
      end
    )
  end

  @spec parse_maps(Map) :: :ok
  def parse_maps_span(maps) do
    maps
    |> Stream.into([])
    |> Stream.map(&parse_map_slug/1)
    |> Stream.run()
  end

  @spec parse_map_slug({String, Map}) :: :ok
  defp parse_map_slug({slug, maps}) do
    Logger.debug("Parsing maps from slug: #{slug}")

    maps
    |> Stream.into([])
    |> Stream.map(&parse_map_community(&1, slug))
    |> Stream.run()
  end

  @spec parse_map_community({String, Map}, String) :: :ok
  defp parse_map_community({community, maps}, slug) do
    Logger.debug("Parsing maps from community: #{community}")

    maps
    |> Stream.into([])
    |> Stream.map(&parse_map_file(&1, slug, community))
    |> Stream.run()
  end

  @spec parse_map_file({String, Map}, String, String) :: %__MODULE__{}
  defp parse_map_file({file, map_json}, slug, community) do
    Logger.debug("Parsing map '#{file}' from #{slug}/#{community}")

    map = %__MODULE__{
      key: {slug, community, file},
      url: Map.get(map_json, "url")
    }

    map = Exneuland.Backend.parse_map_file(map_json, map)

    Memento.transaction(fn -> Memento.Query.write(map) end)
  end
end
