defmodule Exneuland.Store.Variable do
  use Memento.Table, attributes: [:key, :room, :value, :tag], index: [:room]

  @type t :: %__MODULE__{
          key: key,
          room: String,
          value: String,
          tag: String | nil
        }

  @type key :: {String, String}

  @spec fetch!(String, String) :: [t] | nil
  def fetch!(room, name) do
    Memento.transaction!(fn ->
      Memento.Query.read(__MODULE__, {room, name})
    end)
  end

  @spec update(String, String, String, String | nil) :: :ok
  def update(room, name, value, tag \\ nil) do
    record = %__MODULE__{
      key: {room, name},
      room: room,
      value: value,
      tag: tag
    }

    Memento.transaction(fn ->
      Memento.Query.write(record)
    end)

    emit_count()
    :ok
  end

  def get_room(room) do
    guards = [
      {:==, :room, room}
    ]

    Memento.transaction!(fn ->
      Memento.Query.select(__MODULE__, guards)
    end)
  end

  def build_init_messages(room) do
    get_room(room)
    |> Stream.map(fn %__MODULE__{key: {_, name}, value: value} ->
      {:variableMessage, Exneuland.Messages.struct(VariableMessage, name: name, value: value)}
    end)
    |> Enum.into([])
  end

  @spec count_variables() :: number
  def count_variables() do
    Memento.transaction(fn -> Memento.Query.all(__MODULE__) |> Enum.count() end)
    |> case do
      {:ok, count} -> count
      _ -> 0
    end
  end

  @spec emit_count() :: number
  @doc """
  Emmit the current count of maps to telemetry
  """
  def emit_count() do
    count = count_variables()
    :telemetry.execute([:exneuland, :store, :variables], %{count: count})
    count
  end
end
