defmodule Exneuland.VanillaMessages do
  use Exneuland.Messages, files: "./priv/protos/messages.proto", namespace: Exneuland.VanillaMessages


  @impl Exneuland.Messages
  def wrap_batch_s2c(msg) do
    %__MODULE__.ServerToClientMessage{
      message: {
        :batchMessage,
        %__MODULE__.BatchMessage{
          payload: [
            %__MODULE__.SubMessage{
              message: msg
            }
          ]
        }
      }
    }
  end

  @impl Exneuland.Messages
  @spec user_joined_from_state(term) :: struct()
  def user_joined_from_state(state) do
    message = %__MODULE__.UserJoinedMessage{
      userId: state.uid,
      name: state.name,
      characterLayers: state.character,
      position: state.position,
      companion: state.companion,
      userUuid: state.uuid
    }

    with {:ok, vcard} <- Exneuland.Backend.vcard_url(state.uuid) do
      message
      |> Map.put(:visitCardUrl, vcard)
    else
      _ -> message
    end
  end
end
