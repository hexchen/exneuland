defmodule Exneuland.PubSub do
  alias Phoenix.PubSub

  def subscribe(topic, opts \\ []) do
    PubSub.subscribe(__MODULE__, topic, opts)
  end

  def broadcast(topic, msg) do
    PubSub.broadcast_from(__MODULE__, self(), topic, msg)
  end

  def broadcast!(topic, msg) do
    PubSub.broadcast_from!(__MODULE__, self(), topic, msg)
  end

  def send_user(user, msg) do
    broadcast("user:#{user}", msg)
  end

  def send_group(gid, msg) do
    broadcast("group:#{gid}", msg)
  end
end
