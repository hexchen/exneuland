defmodule Exneuland.BackendGen do
  @moduledoc false

  @backend_adapter Application.compile_env(:exneuland, Exneuland.Backend)[:adapter]

  defmacro optional_def_cb(func, args \\ [], return \\ {:error, :not_supported}) do
    args = if is_list(args) do
      args
    else
      [args]
    end
    if Kernel.function_exported?(@backend_adapter, func, Enum.count(args)) do
      {:def, [context: Exneuland.BackendGen, import: Kernel],
        [
          {func, [context: Exneuland.BackendGen],
            unquote_args(args)
          },
          [do: {:apply, [context: Exneuland.BackendGen, import: Kernel], [
            (quote do: @backend_adapter),
            func,
            unquote_args(args)
          ]}]
        ]
      }
    else
      {:def, [context: Exneuland.BackendGen, import: Kernel],
        [
          {func, [context: Exneuland.BackendGen],
            unquote_args(args)
            },
          [do: return]
        ]}
    end
  end

  defp unquote_args(args) do
    args
    |> Enum.map(fn {name, _line, _nil} ->
      {name, [if_undefined: :apply], Elixir}
    end)
  end
end
