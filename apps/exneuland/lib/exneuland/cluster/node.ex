defmodule Exneuland.Cluster.Node do
  require Logger
  use GenServer

  # side note, migrating tables
  # tldr; scale-down to one instance, then :mnesia.transform_table/3
  # see https://elixirschool.com/en/lessons/storage/mnesia for competent info
  @tables [
    Exneuland.Store.User,
    Exneuland.Store.Map,
    Exneuland.Store.Variable
  ]
  @recheck_time 25_000

  def start_link(_ \\ []) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  @spec request_copy(Node, Atom) :: :ok
  def request_copy(node, table) do
    send({__MODULE__, node}, {:copy, table, self()})
    :ok
  end

  @spec request_table(Atom) :: :ok
  def request_table(table) do
    node = Enum.random(Node.list())

    if Node.ping(node) == :pong do
      request_copy(node, table)
    else
      request_table(table)
    end
  end

  def create_tables_force() do
    GenServer.call(__MODULE__, :create_tables)
  end

  def query_registry(registry, key), do: query_registry(Node.self(), registry, key)

  def query_registry(node, registry, key) do
    GenServer.call({__MODULE__, node}, {:registry_query, registry, key})
  end

  def query_all(registry, key) do
    [Node.self() | Node.list()]
    |> Enum.map(&query_registry(&1, registry, key))
    |> List.foldl([], &++/2)
  end

  def is_ready() do
    GenServer.call(__MODULE__, {:is_ready})
  end

  def get_state(node \\ Node.self()) do
    GenServer.call({__MODULE__, node}, {:get_state})
  end

  def get_state_all() do
    [Node.self() | Node.list()]
    |> Enum.map(fn node ->
      {node, Exneuland.Cluster.Node.get_state(node)}
    end)
  end

  @spec wait_tables(number) :: :ok | {:error, [Atom]}
  def wait_tables(timeout \\ 200) do
    Memento.Table.wait(@tables, timeout)
  end

  @impl GenServer
  def init(_) do
    Process.sleep(1000)
    :net_kernel.monitor_nodes(true)
    Memento.add_nodes(Node.list())
    Process.flag(:trap_exit, true)

    state = init_copy()

    {:ok, state}
  end

  # query the local registry and pass the processes back
  @impl GenServer
  def handle_call({:registry_query, registry, key}, _sender, state) do
    {:reply, Registry.lookup(registry, key), state}
  end

  @impl GenServer
  def handle_call({:is_ready}, _sender, state) do
    ready? =
      @tables
      |> Enum.map(fn table ->
        Map.get(state, table)
      end)
      |> Enum.filter(fn v -> v == :ok end)
      |> Enum.count() == Enum.count(@tables)

    {:reply, ready?, state}
  end

  @impl GenServer
  def handle_call({:get_state}, _sender, state) do
    {:reply, state, state}
  end

  @impl GenServer
  def handle_call(:create_tables, _sender, state) do
    create_new_tables()

    @tables
    |> Stream.map(fn table ->
      ref = Map.get(state, table)

      if is_reference(ref) do
        Process.cancel_timer(ref, async: true)
      end
    end)

    state =
      @tables
      |> Enum.reduce(%{}, fn table, acc -> Map.put(acc, table, :ok) end)

    Logger.debug("state: #{state}")


    {:reply, :ok, state}
  end

  @impl GenServer
  # Allow remote dispatching from other hosts
  def handle_cast({:dispatch, registry, key, message}, state) do
    Registry.dispatch(
      registry,
      key,
      fn entries ->
        for x <- entries do
          send(x, message)
        end
      end
    )

    {:noreply, state}
  end

  @impl GenServer
  def handle_info({:copy_done, :ok, table}, state) do
    Logger.info("Copied table #{inspect(table)}: :ok")

    ref =
      state
      |> Map.get(table)

    if is_reference(ref) do
      Process.cancel_timer(ref, async: true)
    end

    state =
      state
      |> Map.put(table, :ok)

    {:noreply, state}
  end

  @impl GenServer
  def handle_info({:copy_done, :error, table}, state) do
    Logger.warn("Copied table #{inspect(table)}: :error")

    ref =
      state
      |> Map.get(table)

    state =
      if ref == :ok do
        state
      else
        if is_reference(ref) do
          Process.cancel_timer(ref, async: true)
        end

        Process.send_after(self(), {:request_copy, table}, 2_000 + Enum.random(1..2_000))
        ref = Process.send_after(self(), {:request_copy, table}, @recheck_time)

        state
        |> Map.put(table, ref)
      end

    {:noreply, state}
  end

  @impl GenServer
  # request for tables to be copied
  def handle_info({:copy, table, pid}, state) do
    with :ok <- Map.get(state, table),
         :ok <- Memento.Table.create_copy(table, node(pid), :ram_copies) do
      send(pid, {:copy_done, :ok, table})
    else
      _ ->
        send(pid, {:copy_done, :error, table})
    end

    {:noreply, state}
  end

  @impl GenServer
  def handle_info({:request_copy, table}, state) do
    request_table(table)
    {:noreply, state}
  end

  @impl GenServer
  def handle_info({:nodeup, node}, state) do
    Memento.add_nodes(node)
    {:noreply, state}
  end

  @impl GenServer
  def handle_info({:nodedown, _node}, state) do
    {:noreply, state}
  end

  @impl GenServer
  def handle_info(_, state), do: {:noreply, state}

  @impl GenServer
  def terminate(_reason, _state) do
    Phoenix.PubSub.local_broadcast(Exneuland.PubSub, "rooms:all", {:terminate_node})
    Process.sleep(30_000)
    Memento.stop()
    System.stop()
    false
  end

  @spec init_copy() :: Map
  defp init_copy() do
    # this can potentially cause split-brain and break stuff badly, requiring a full cluster restart.
    # let's hope it never happens
    case Node.list() do
      [] ->
        create_new_tables()
        Logger.debug("Tables created")

        @tables
        |> Enum.reduce(%{}, fn table, acc -> Map.put(acc, table, :ok) end)

      _ ->
        Enum.each(@tables, &request_table/1)
        %{}
    end
  end

  defp create_new_tables() do
    Task.start(fn ->
      with {:timeout, tables} when is_list(tables) <- wait_tables() do
        Logger.warn("No other nodes, creating tables: #{inspect(tables)}")
        Enum.each(tables, &Memento.Table.create!/1)
      else
        _ ->
          Logger.warn("Table create requests but all tables do exists")
          :ok
      end
    end)

    :ok

    # TODO: sync map somehow after boot if tables are empty
    # Logger.info("Requesting maps from hub")

    # Task.Supervisor.async_nolink(Exneuland.Hub, fn ->
    #  Exneuland.Hub.refresh_maps()
    #  |> case do
    #    {:ok, count} ->
    #      Logger.info("Got #{count} Maps from hub")

    #    {:error, err} ->
    #      Logger.warn("Got error while refreshing maps: #{inspect(err)}")
    #  end
    # end)
  end
end
