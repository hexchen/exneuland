defmodule Exneuland.Cluster.ReadyState do
  use GenServer

  def start_link(_ \\ []) do
    GenServer.start_link(__MODULE__, false, name: __MODULE__)
  end

  @spec is_ready?(Node, number) :: bool
  def is_ready?(node \\ Node.self(), timeout \\ 6_000) do
    GenServer.call({__MODULE__, node}, :is_ready, timeout)
  end

  @spec is_ready_all?(number) :: [{Node, bool}]
  def is_ready_all?(timeout \\ 600) do
    [Node.self() | Node.list()]
    |> Stream.map(fn node ->
      {node, is_ready?(node, timeout)}
    end)
  end

  @impl GenServer
  def init(_) do
    {:ok, false}
  end

  @impl GenServer
  def handle_call(:is_ready, _sender, state) do
    state =
      if state do
        :ok == Exneuland.Cluster.Node.wait_tables()
      else
        try do
          GenServer.call(Exneuland.Cluster.Node, {:is_ready}, 5_000)
        rescue
          _ ->
            false
        end
      end

    {:reply, state, state}
  end

  @impl GenServer
  # Handle stale messages
  def handle_info({ref, _value}, state) when is_reference(ref) do
    {:noreply, state}
  end
end
