defmodule Exneuland.Token do
  use Joken.Config
  alias Exneuland.Store.User

  @default_exp 60 * 60 * 24 * 7
  @backend_aud "Exneuland API"
  @uuid_claim "userUuid"

  @spec token_config() :: Joken.token_config()
  def token_config do
    default_claims(default_exp: @default_exp, aud: "WorkAdventure", iss: "ExNeuland")
  end

  @spec get_signer() :: Joken.Signer
  def get_signer() do
    Joken.Signer.create("HS256", Application.fetch_env!(:exneuland, :token_secret))
  end

  @spec verify_user_token(String) ::
          {:ok, Joken.claims(), Exneuland.Store.User}
          | {:error, Joken.error_reason()}
          | {:error, atom}
  @doc """
  Verify the token and fetch the user owning the token
  """
  def verify_user_token(token) do
    with {:ok, claims} <- verify_and_validate(token, get_signer()),
         userid when userid != nil <- claims[@uuid_claim],
         user when user != nil <- User.fetch_or_invalidate(userid) do
      if user.active, do: {:ok, claims, user}, else: {:error, {:inactive, claims, user}}
    else
      {:error, error} ->
        {:error, error}

      _ ->
        {:error, :invalid}
    end
  end

  def generate_user_token(%Exneuland.Store.User{uuid: uuid}) do
    generate_user_token(uuid)
  end

  def generate_user_token(uuid) when is_binary(uuid) do
    generate_and_sign(%{@uuid_claim => uuid}, get_signer())
  end

  @spec verify_backend_token(String) ::
          {:ok, Joken.claims()}
          | {:error, Joken.error_reason()}
          | {:error, atom}
  @doc """
  Verify Token to be valid for backend authentication
  """
  def verify_backend_token(token) do
    # FIXME: make sure token is valid, not only aud
    with {:ok, claims} <- verify(token, get_signer()) do
      if claims["aud"] == @backend_aud do
        {:ok, claims}
      else
        {:ok, :invalid}
      end
    else
      err -> err
    end
  end

  def generate_backend_token() do
    generate_and_sign(%{"aud" => @backend_aud}, get_signer())
  end
end
