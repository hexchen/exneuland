defmodule Exneuland.Backend.Report.User do
  @derive Jason.Encoder
  defstruct [
    :reportedUserUuid,
    :reportedUserIpAddress,
    :reporterUserUuid,
    :orgSlug,
    :worldSlug,
    :roomSlug,
    :comment
  ]

  @type t() :: %__MODULE__{
          reportedUserUuid: String,
          reportedUserIpAddress: String,
          reporterUserUuid: String,
          orgSlug: String,
          worldSlug: String,
          roomSlug: String,
          comment: String | nil
        }
end
