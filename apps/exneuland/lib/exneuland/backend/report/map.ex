defmodule Exneuland.Backend.Report.Map do
  @derive Jason.Encoder
  defstruct [
    :mapUrl,
    :orgSlug,
    :worldSlug,
    :roomSlug,
    :reporterUserUuid,
    :comment,
    :position
  ]

  @type t() :: %__MODULE__{
          mapUrl: String,
          orgSlug: String,
          worldSlug: String,
          roomSlug: String,
          reporterUserUuid: String,
          comment: String | nil,
          position: %{
            x: number,
            y: number
          }
        }
end
