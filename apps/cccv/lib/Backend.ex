defmodule CCCV.Exneuland.Backend do
  use Exneuland.Backend
  require Logger
  alias Exneuland.Backend
  alias CCCV.Exneuland.Badge
  alias CCCV.Exneuland.User.UserConfig
  alias Exneuland.PubSub
  import CCCV.Exneuland.Hub, only: [event_slug: 0]

  @http_backend CCCV.Exneuland.Hub

  @spec fetch_user_uuid(String) :: {:ok, Backend.user()} | {:error, term}
  @impl Exneuland.Backend
  def fetch_user_uuid(uuid) do
    with {:ok, %HTTPoison.Response{status_code: 200, body: body}} <-
           @http_backend.get("/api/c/#{event_slug()}/workadventure/checkuser/#{uuid}") do
      {:ok,
       %Exneuland.Store.User{
         uuid: uuid,
         nickname: body["nickname"],
         color: body["color"],
         messages: body["messages"],
         textures: body["textures"],
         layers: body["characterLayers"],
         badges: [],
         config: UserConfig.parse_user_info_body(body["wa_userconfig"]),
         active: true
       }}
    else
      {:ok, %HTTPoison.Response{status_code: code}} -> {:error, {:status_code, code}}
      {:error, error} -> {:error, error}
    end
  end

  @spec refresh_maps() :: {:ok, number} | {:error, term}
  @impl Exneuland.Backend
  def refresh_maps() do
    with {:ok, %HTTPoison.Response{status_code: 200, body: body}} <-
           @http_backend.get("/api/c/#{event_slug()}/workadventure/maps/list") do
      {:ok, Exneuland.Store.Map.parse_maps(body)}
    else
      {:ok, %HTTPoison.Response{status_code: status_code}} ->
        :telemetry.execute([:exneuland, :hub, :error], %{}, %{code: status_code, path: :maps_list})

        {:error, {:status_code, status_code}}

      {:error, err} ->
        {:error, err}
    end
  end

  @impl Exneuland.Backend
  def parse_map_file(map_json, map) do
    badges = CCCV.Exneuland.Badge.parse_badge_list(Map.get(map_json, "badges", []))

    map
    |> Map.put(:badges, badges)
  end

  @spec register_user(String) :: {:ok, Backend.user(), String} | {:error, term}
  @impl Exneuland.Backend
  def register_user(token) do
    with {:ok, %HTTPoison.Response{status_code: 200, body: body}} <-
           @http_backend.get("/api/c/#{event_slug()}/workadventure/login/#{token}"),
         {:ok, user} <- parse_user(body) do
      {:ok, user, "/@/#{body["organizationSlug"]}/#{body["worldSlug"]}/#{body["roomSlug"]}"}
    else
      {:ok, %HTTPoison.Response{status_code: status_code}} ->
        :telemetry.execute([:exneuland, :hub, :error], %{}, %{code: status_code, path: :login})

        {:error, {:status_code, status_code}}

      {:error, err} ->
        {:error, err}
    end
  end

  @spec parse_user(Map) :: {:ok, Backend.user()} | {:error, term}
  @impl Exneuland.Backend
  def parse_user(body) do
    {:ok,
     %Exneuland.Store.User{
       uuid: body["userUuid"],
       nickname: body["nickname"],
       color: body["color"],
       messages: body["messages"],
       textures: body["textures"],
       layers: body["characterLayers"],
       # tags: body["tags"],
       badges: [],
       config: UserConfig.parse_user_info_body(body["wa_userconfig"]),
       active: true
     }}
  end

  @spec update_user(Backend.user() | String, Map) :: :ok | {:error, term}
  @impl Exneuland.Backend
  def update_user(%Exneuland.Store.User{} = user, update) do
    update_user(user.uuid, update)
  end

  @impl Exneuland.Backend
  def update_user(user, update) when is_binary(user) do
    Task.Supervisor.async_nolink(CCCV.Exneuland.Hub, fn ->
      send_update(user, update)
    end)

    :ok
  end

  def send_update(user_uuid, update) when is_binary(user_uuid) do
    update =
      update
      |> Map.put("userUuid", user_uuid)
      |> Jason.encode!()

    headers = [{"content-type", "application/json"}]

    with {:ok, %HTTPoison.Response{status_code: status_code}}
         when status_code >= 200 and status_code < 300 <-
           @http_backend.post(
             "/api/c/#{event_slug()}/workadventure/checkuser/#{user_uuid}",
             update,
             headers
           ) do
      :ok
    else
      {:ok, %HTTPoison.Response{status_code: status_code}} ->
        :telemetry.execute([:exneuland, :hub, :error], %{}, %{
          code: status_code,
          path: :checkuser_post
        })

        {:error, {:status_code, status_code}}

      {:error, err} ->
        {:error, err}
    end
  end

  @spec report_user(Backend.user_report()) :: :ok | {:error, term}
  @impl Exneuland.Backend
  def report_user(user) do
    report = Jason.encode!(user)
    headers = [{"content-type", "application/json"}]

    with {:ok, %HTTPoison.Response{status_code: 204}} <-
           @http_backend.post("/api/c/#{event_slug()}/workadventure/report/user", report, headers) do
      :ok
    else
      {:ok, %HTTPoison.Response{status_code: status_code}} ->
        :telemetry.execute([:exneuland, :hub, :error], %{}, %{
          code: status_code,
          path: :report_user
        })

        {:error, {:status_code, status_code}}

      {:error, err} ->
        {:error, err}
    end
  end

  @spec report_map(Backend.map_report()) :: :ok | {:term, term}
  @impl Exneuland.Backend
  def report_map(report) do
    report = Jason.encode!(report)
    headers = [{"content-type", "application/json"}]

    with {:ok, %HTTPoison.Response{status_code: 204}} <-
           @http_backend.post("/api/c/#{event_slug()}/workadventure/report/map", report, headers) do
      :ok
    else
      {:ok, %HTTPoison.Response{status_code: status_code}} ->
        :telemetry.execute([:exneuland, :hub, :error], %{}, %{
          code: status_code,
          path: :report_map
        })

        {:error, {:status_code, status_code}}

      {:error, err} ->
        {:error, err}
    end
  end

  @impl Exneuland.Backend
  def metrics do
    [
      Telemetry.Metrics.counter("exneuland.hub.error.count", tags: [:code, :path])
    ]
  end

  @impl Exneuland.Backend
  def handle_message({:userMovesMessage, msg} = cmsg, state) do
    {:ok, state} = Exneuland.MessageHandler.handle(cmsg, state)

    badges_at_pos =
      Badge.check_badge_pos(msg.position, state.private.badges)
      |> found_badges(state.uuid)

    {_, state} =
      state
      |> Map.get_and_update(:private, fn old ->
        {_, private} =
          old
          |> Map.get_and_update(:badges, fn old ->
            badges = old -- badges_at_pos
            {old, badges}
          end)

        {old, private}
      end)

    {:ok, state}
  end

  @impl Exneuland.Backend
  def handle_backend({:badge, {:ok, msg, icon}}, state) do
    msg = Exneuland.Messages.struct(SendUserMessage,
      type: "message",
      message:
        Jason.encode!(%{
          ops: [
            %{insert: %{image: icon}, attributes: %{width: "64px"}},
            %{insert: "Found badge: #{msg}"}
          ]
        })
    )

    msg = Exneuland.Messages.s2c({:sendUserMessage, msg})

    Exneuland.Messages.send_proto(msg, state)
  end

  @impl Exneuland.Backend
  # Already has the badge
  def handle_backend({:badge, {:ok, :already_owned}}, state) do
    {:ok, state}
  end

  @impl Exneuland.Backend
  # Error already printed
  def handle_backend({:badge, {:error, _}}, state), do: {:ok, state}

  @impl Exneuland.Backend
  def supervisor_children() do
    [
      # Hub Async requests
      {Task.Supervisor, name: CCCV.Exneuland.Hub},
      ## Badge tasks
      {Task.Supervisor, name: CCCV.Exneuland.Hub.BadgeRedeemTasks}
    ]
  end

  @impl Exneuland.Backend
  def init_state(state, _user, map, _args) do
    private = %{
      badges: map.badges
    }

    state =
      state
      |> Map.put(:private, private)

    {:ok, state}
  end

  @impl Exneuland.Backend
  def vcard_url(uuid) when is_binary(uuid) do
    Exneuland.BackendConfig.fetch_env(:vcard_base_url)
    |> case do
      nil -> {:error, :not_configured}
      base -> {:ok, base <> uuid}
    end
  end

  # Redeem badge code
  @spec found_badges([Badge], String) :: :ok
  def found_badges(badges, user) do
    badges
    |> Stream.map(fn badge ->
      Task.Supervisor.async(CCCV.Exneuland.Hub.BadgeRedeemTasks, __MODULE__, :redeem_badge, [
        badge,
        user
      ])
    end)
    |> Stream.run()

    badges
  end

  def redeem_badge(%CCCV.Exneuland.Badge{} = badge, user) do
    msg =
      redeem_badge(badge.badge_id, user)
      |> case do
        {:ok, msg_text, icon} = msg when is_binary(msg_text) and is_binary(icon) ->
          {:badge, msg}

        {:ok, _} = msg ->
          {:badge, msg}

        {:error, err} ->
          Logger.warn("Failed to redeem badge: #{inspect(err)}")
          {:badge, :error}
      end

    PubSub.send_user(user, {:backend, msg})
  end

  def redeem_badge(token, user) when is_binary(user) do
    with {:ok, %HTTPoison.Response{status_code: status_code, body: body}}
         when status_code == 201 or status_code == 204 <-
           @http_backend.post(
             "/api/c/#{event_slug()}/workadventure/redeem_badge/#{token}",
             {:form, [{"user", "#{user}"}]}
           ) do
      if status_code == 201 do
        msg =
          body
          |> Map.get("msg")

        icon =
          body
          |> Map.get("icon")

        {:ok, msg, icon}
      else
        {:ok, :already_owned}
      end
    else
      {:ok, %HTTPoison.Response{status_code: status_code}} ->
        :telemetry.execute([:exneuland, :hub, :error], %{}, %{
          code: status_code,
          path: :redeem_badge
        })

        {:error, {:status_code, status_code}}

      {:error, err} ->
        {:error, err}
    end
  end
end
