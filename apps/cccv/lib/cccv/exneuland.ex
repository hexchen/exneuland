defmodule CCCV.Exneuland do
  @moduledoc """
  Documentation for `CCCV.Exneuland`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> CCCV.Exneuland.hello()
      :world

  """
  def hello do
    :world
  end
end
