defmodule CCCV.Exneuland.User.UserConfig do
  require Logger

  @derive Jason.Encoder
  defstruct blockExternalContent: false,
            blockAmbientSounds: false,
            disableCamera: false,
            forceWebsiteTrigger: false,
            ignoreFollowRequests: false,
            noAnimations: false,
            silentMode: false

  @known_fields [
    "blockExternalContent",
    "blockAmbientSounds",
    "disableCamera",
    "forceWebsiteTrigger",
    "ignoreFollowRequests",
    "noAnimations",
    "silentMode"
  ]

  @type t() :: %__MODULE__{
          blockExternalContent: boolean,
          blockAmbientSounds: boolean,
          disableCamera: boolean,
          forceWebsiteTrigger: boolean,
          ignoreFollowRequests: boolean,
          noAnimations: boolean,
          silentMode: boolean
        }

  @spec parse_user_info_body(Map | nil) :: t | nil
  def parse_user_info_body(nil) do
    nil
  end

  def parse_user_info_body(body) do
    body =
      body
      |> Map.take(@known_fields)
      |> Enum.map(fn {k, v} -> {String.to_existing_atom(k), v} end)

    struct(__MODULE__, body)
  end

  @spec build_user_settings_wa(String, t | nil) :: Map
  @doc """
  Convert CCCV UserSettings to WA protobuf message
  """
  def build_user_settings_wa(nick, config) when config != nil do
    config =
      config
      |> Map.from_struct()
      |> Enum.map(fn {k, v} ->
        if k == :disableCamera do
          {:noVideo, v}
        else
          {k, v}
        end
      end)

    # %Workadventure.Messages.UserSettings{
    #    nickname: nick,
    # }
    struct(Workadventure.Messages.UserSettings, config)
    |> Map.put(:nickname, nick)
  end

  def build_user_settings_wa(_nick, nil) do
    nil
  end

  @spec build_user_settings_hub(Map) :: t
  @doc """
  Convert Workadventure protobuf message to CCCV UserConfig
  """
  def build_user_settings_hub(config) do
    config =
      config
      |> Map.from_struct()
      |> Enum.map(fn {k, v} ->
        if k == :noVideo do
          {:disableCamera, v}
        else
          {k, v}
        end
      end)

    struct(__MODULE__, config)
  end
end
