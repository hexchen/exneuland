defmodule CCCV.Exneuland.Badge do
  require Logger

  defstruct x: nil,
            y: nil,
            size: nil,
            badge_id: nil

  @type t() :: %__MODULE__{
          x: number,
          y: number,
          size: number | {number, number},
          badge_id: String
        }

  @spec parse_badge_list(Map) :: __MODULE__
  def parse_badge_list([
        %{
          "width" => width,
          "height" => height,
          "type" => "rectangle",
          "x" => x,
          "y" => y,
          "token" => token
        }
        | tail
      ]) do
    [
      %__MODULE__{
        x: x,
        y: y,
        size: {width, height},
        badge_id: token
      }
      | parse_badge_list(tail)
    ]
  end

  def parse_badge_list([%{"type" => "point", "x" => x, "y" => y, "token" => token} | tail]) do
    [
      %__MODULE__{
        x: x,
        y: y,
        size: 32 + 16,
        badge_id: token
      }
      | parse_badge_list(tail)
    ]
  end

  def parse_badge_list([%{} = badge | tail]) do
    Logger.warn("Could not parse badge: #{inspect(badge)}")
    parse_badge_list(tail)
  end

  def parse_badge_list([]), do: []

  # @spec check_badge_pos(Exneuland.SocketHandler.position(), [t]) :: [t]
  @doc """
  Check if any badge is in reach of `pos`
  """
  def check_badge_pos(pos, badges) do
    badges
    |> Stream.filter(fn badge -> is_in_range(pos, badge) end)
    |> Enum.into([])
  end

  # @spec is_in_range(Exneuland.SocketHandler.position(), t) :: boolean
  @doc """
  Check if the position is inside the badge box
  """
  def is_in_range(position, badge)

  def is_in_range(%{x: px, y: py}, %__MODULE__{
        size: {with, height},
        x: bx,
        y: by
      }) do
    cond do
      px < bx || py < by -> false
      px > bx + with || py > by + height -> false
      true -> true
    end
  end

  def is_in_range(%{x: px, y: py}, %__MODULE__{
        size: radius,
        x: bx,
        y: by
      }) do
    cond do
      px < bx - radius / 2 || py < by - radius / 2 -> false
      px > bx + radius / 2 || py > by + radius / 2 -> false
      true -> true
    end
  end

  # FIXME: Compatibility shim, remove after full deploy
  def is_in_range(player, %{x: x, y: y, size: size}) do
    is_in_range(player, %__MODULE__{x: x, y: y, size: size})
  end
end
