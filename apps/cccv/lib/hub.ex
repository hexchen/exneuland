defmodule CCCV.Exneuland.Hub do
  require Logger
  use HTTPoison.Base
  alias Exneuland.BackendConfig

  @spec hub_url() :: binary
  @doc """
  Load the hub url from config.
  """
  def hub_url, do: BackendConfig.fetch_env!(:hub_url)
  @spec hub_token() :: binary
  @doc """
  Load the hub token from config.
  """
  def hub_token, do: BackendConfig.fetch_env!(:hub_token)

  @spec event_slug() :: binary
  @doc """
  Load the event slug from config.
  """
  def event_slug, do: BackendConfig.fetch_env!(:event_slug)

  @impl HTTPoison.Base
  @spec process_request_url(binary) :: binary
  def process_request_url(url) do
    hub_url() <> url
  end

  @impl HTTPoison.Base
  @spec process_request_headers(term) :: [{binary, term}]
  def process_request_headers(headers) do
    [
      {"Authorization", hub_token()}
      | Enum.filter(headers, fn {header, _value} -> String.downcase(header) != "authorization" end)
    ]
  end

  @impl HTTPoison.Base
  @spec process_response_body(binary) :: term
  def process_response_body(body) do
    case Jason.decode(body) do
      {:ok, value} ->
        value

      # |> Map.take(@expected_fields)
      # |> Enum.map(fn {k, v} -> {String.to_atom(k), v} end)

      _ ->
        body
    end
  end

  @impl HTTPoison.Base
  @spec process_response(HTTPoison.Base.response()) :: any
  def process_response(%HTTPoison.Response{status_code: status} = resp)
      when status >= 300 do
    Logger.error("Hub request failed: #{inspect(resp)}")
    resp
  end

  @impl HTTPoison.Base
  @spec process_response(HTTPoison.Base.response()) :: any
  def process_response(response), do: response
end
