# Exneuland

Elixir reimplementation of the workadventure backend. Docs can be found in `docs/`.

## Deployment Pipeline

If you push a commit, it will be automatically linted, tested, and documentation will be created by CI.
After that the Docker Image is built and the image pushed to the GitLab Docker Registry with the image tag consisting of

* the short git commit sha
* the pipeline id in which the image was built

After that, a new branch in the [Deployment Repository][1] is created and the image-tag of the exneuland Deployment is swapped to the one just built in order to re-deploy the Container in Kubernetes.
After pushing that branch a MR is opened that needs review and approval from a maintainer, before it can be merged.
Once the MR created is reviewed and merged, the new Docker Image is deployed to the Kubernetes cluster in a matter of a few minutes.

If you push a tag to the main branch, there will be two merge requests opened in the [Deployment Repository][1], one to deploy to staging and one to deploy to production.

[1]: https://git.cccv.de/hub/exneuland-deploy

## Devcontainer

This Repo comes with a [devcontainer for VSCode][2].
If you use VSCode, make sure, you have the `ms-azuretools.vscode-docker` (Docker) extension installed as well as Docker installed on your host. Then you should be able to klick in the lower-left corner and then select "Reopen in Container".
This will re-start your VSCode inside of a Dockercontainer with VSCode elexir extensions isntalled and a fully pre-configured elexir environment to your fingertips using the integrated Terminal of vscode.

[2]: https://www.aaron-powell.com/posts/2021-03-08-your-open-source-project-needs-a-dev-container-heres-why/
