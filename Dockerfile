FROM registry.git.cccv.de/hub/exneuland/elixir:1.12 AS builder

ENV MIX_ENV=prod

WORKDIR /usr/local/exneuland

# This step installs all the build tools we'll need
RUN mix local.rebar --force && \
    mix local.hex --force && \
    apt update && apt install -y protobuf-compiler

COPY mix.* ./

# Compile Elixir
RUN mix do deps.get, deps.compile

COPY . .

# Build Release
RUN mkdir -p /opt/release \
    && mix do sentry_recompile, release \
    && mv _build/${MIX_ENV}/rel/exneuland /opt/release

# Create the runtime container
FROM registry.git.cccv.de/hub/exneuland/erlang:24 as runtime

WORKDIR /usr/local/exneuland

COPY --from=builder /opt/release/exneuland .

CMD [ "bin/exneuland", "start" ]
